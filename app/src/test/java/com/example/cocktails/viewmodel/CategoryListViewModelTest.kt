package com.example.cocktails.viewmodel

import com.example.cocktails.model.entity.Drink
import com.example.cocktails.model.remote.ApiService
import com.example.cocktails.model.repository.RepositoryImpl
import com.example.cocktails.model.retrofit.RetrofitClass
import com.example.cocktails.util.Resource
import com.example.cocktails.utilTest.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class CategoryListViewModelTest{
    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()
    private val mockService = mockk<ApiService>() // what we are trying to test but is unreachable
    //therefore we need to mocck the object
    private val mockRetrofit = mockkObject(RetrofitClass)

    private val repo = mockk<RepositoryImpl>()

    @BeforeEach
    fun beforeEach(){
        mockkObject(RetrofitClass)
        every { RetrofitClass.getApiService() } answers  {mockService}
    }

    //Given

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    @DisplayName("Testing if initial list is being retrieved")
    fun testViewModel() = runTest(coroutinesTestExtension.dispatcher) {
        // Given
        val drinks = listOf(
            Drink(
            strCategory = ""
            )
        )
        val testDrinks = repo.getCategoryList()
       when (testDrinks) {
            is Resource.Error -> "Error"
            Resource.Loading -> "Loading"
            is Resource.Success -> testDrinks.data
           else -> {}
       }

//        coEvery { testDrinks} coAnswers {  }

        // When
        val vm = CategoryListViewModel()
        val state = vm.categoryList

        // Then
        assertFalse(state.value.equals(null))
//        assertEquals(testDrinks, state.value)

    }
}
